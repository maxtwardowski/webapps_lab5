import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  n: number;
  fibVal: number;

  constructor() {
    this.n = 0;
    this.fibVal = 0;
  }

  submitFib(nth: HTMLInputElement): boolean {
    this.n = parseInt(nth.value)
    this.fibVal = this.calculateFib(this.n);
    return false;
  }

  calculateFib(n: number): number {
    if (n == 0)
      return 0;
    else if (n == 1)
      return 1;
    else
      return this.calculateFib(n - 1) + this.calculateFib(n - 2);
  }

  clear(): boolean {
    this.n = 0;
    this.fibVal = 0;
    return false;
  }

}
